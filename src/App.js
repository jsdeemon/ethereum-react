import './App.css';
import { useState } from 'react';
import { ethers } from 'ethers'
import Greeter from './artifacts/contracts/Greeter.sol/Greeter.json'
import Token from './artifacts/contracts/Token.sol/Token.json'

const greeterAddress = ""
const tokenAddress = ""

function App() {
  const [greeting, setGreetingValue] = useState()
  const [userAccount, setUserAccount] = useState()
  const [amount, setAmount] = useState()

  async function requestAccount() {
    await window.ethereum.request({ method: 'eth_requestAccounts' });
  }

  async function fetchGreeting() {
    if (typeof window.ethereum !== 'undefined') {
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      console.log({ provider })
      const contract = new ethers.Contract(greeterAddress, Greeter.abi, provider)
      try {
        const data = await contract.greet()
        console.log('data: ', data)
      } catch (err) {
        console.log("Error: ", err)
      }
    } else {
      alert("Your web browser does not have metamask!")
    }     
  }

  async function getBalance() {
    if (typeof window.ethereum !== 'undefined') {
      const [account] = await window.ethereum.request({ method: 'eth_requestAccounts' })
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const contract = new ethers.Contract(tokenAddress, Token.abi, provider)
      const balance = await contract.balanceOf(account);
      console.log("Balance: ", balance.toString());
    }
  }

  async function setGreeting() {
    if (!greeting) return
    if (typeof window.ethereum !== 'undefined') {
      await requestAccount()
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      console.log({ provider })
      const signer = provider.getSigner()
      const contract = new ethers.Contract(greeterAddress, Greeter.abi, signer)
      const transaction = await contract.setGreeting(greeting)
      await transaction.wait()
      fetchGreeting()
    }
  }

  async function sendCoins() {
    if (typeof window.ethereum !== 'undefined') {
      await requestAccount()
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const signer = provider.getSigner();
      const contract = new ethers.Contract(tokenAddress, Token.abi, signer);
      const transaction = await contract.transfer(userAccount, amount);
      await transaction.wait();
      console.log(`${amount} Coins successfully sent to ${userAccount}`);
    }
  }

  return (
    <div className="container-fluid">
<div className="col-md-12 maindiv">
  <h1 className='text-center'>Ethereum web app</h1>
  <div className="mb-3">
    <button 
     onClick={fetchGreeting}
    className='btn btn-primary'>Fetch Greeting</button>
  </div>
  
<div className="mb-3">
  <input
  onChange={e => setGreetingValue(e.target.value)}
  value={greeting}
  type="text" className="form-control" placeholder="input greeting text" />
</div>
<div className="mb-3">
    <button 
    onClick={setGreeting}
    className='btn btn-success'>Send Greeting</button>
  </div>

  <div className="mb-3">
    <button 
    onClick={getBalance}
    className='btn btn-secondary'>Get balance</button>
  </div>
  <div className="mb-3">
  <input className="form-control" onChange={e => setUserAccount(e.target.value)} placeholder="Account ID" />
    </div>

    <div className="mb-3">
    <input className="form-control" onChange={e => setAmount(e.target.value)} placeholder="Amount" />
    </div>

    <div className="mb-3">
    <button 
    onClick={sendCoins}
    className='btn btn-warning'>Send coins</button>
  </div>

</div>

      {/* <header className="App-header">
        <button onClick={fetchGreeting}><i className="bi bi-0-circle"></i>Fetch Greeting</button>
        <button onClick={setGreeting}>Set Greeting</button>
        <input onChange={e => setGreetingValue(e.target.value)} placeholder="Set greeting" />

        <br />
        <button onClick={getBalance}>Get Balance</button>
        <button onClick={sendCoins}>Send Coins</button>
        <input onChange={e => setUserAccount(e.target.value)} placeholder="Account ID" />
        <input onChange={e => setAmount(e.target.value)} placeholder="Amount" />
      </header> */}

    </div>
  );
}

export default App;
